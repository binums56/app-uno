import React, { Component } from "react";
import { withAuthenticator } from "aws-amplify-react";
import { API, graphqlOperation } from "aws-amplify";
import { createNote, deleteNote, updateNote } from "./graphql/mutations";
import { listNotes } from "./graphql/queries";

class App extends Component {
  state = {
    id: "",
    note: "",
    notes: []
  };

  isExistingNote = () => {
    const { notes, id } = this.state;
    if (id) {
      const isNote = notes.findIndex(note => note.id === id) > -1;
      return isNote;
    }
    return false;
  };
  handleNoteChange = event => this.setState({ note: event.target.value });
  handleNoteAdd = async event => {
    event.preventDefault();
    if (this.isExistingNote()) {
      this.handleNoteUpdate();
    } else {
      const input = { note: this.state.note };
      const result = await API.graphql(graphqlOperation(createNote, { input }));
      const newNote = result.data.createNote;
      const updatedNote = [newNote, ...this.state.notes];
      this.setState({ notes: updatedNote, note: "" });
    }
  };

  componentDidMount = async () => {
    const result = await API.graphql(graphqlOperation(listNotes));
    this.setState({ notes: result.data.listNotes.items });
  };
  handleNoteDelete = async noteId => {
    const { notes } = this.state;
    const input = { id: noteId };
    const result = await API.graphql(graphqlOperation(deleteNote, { input }));
    const deletedNodeId = result.data.deleteNote.id;
    const updatedNote = notes.filter(note => note.id !== deletedNodeId);
    this.setState({ notes: updatedNote });
  };
  // handleNoteUpdate = async newNote => {
  //   const { notes } = this.state;
  //   const result = await API.graphql(graphqlOperation(updateNote, { newNote }));
  //   const { updatedNoteId, updatedNoteNote } = result.data.updateNote;
  //   const changedNote = notes.filter(note => {
  //     if (note.id === updatedNoteId) note.note = updatedNoteNote;
  //   });
  //   this.setState({ notes: changedNote });
  // };

  handleSetNote = ({ note, id }) => this.setState({ note, id });
  handleNoteUpdate = async () => {
    const { notes, note, id } = this.state;
    const input = { id, note };
    const result = await API.graphql(graphqlOperation(updateNote, { input }));
    const updatedNote = result.data.updateNote;
    const index = notes.findIndex(note => note.id === updatedNote.id);
    const updatedNotes = [
      ...notes.slice(0, index),
      updatedNote,
      ...notes.slice(index + 1)
    ];
    this.setState({ notes: updatedNotes, id: "", note: "" });
  };

  render() {
    const { notes } = this.state;
    return (
      <div className="flex flex-column items-center justify-center pa3 bg-washed-red">
        <h1 className="code f2">Amplify Notemaker</h1>
        <form className="mb3" onSubmit={this.handleNoteAdd}>
          <input
            type="text"
            className="pa2 f4 code"
            placeholder="Write ur note here"
            onChange={this.handleNoteChange}
            value={this.state.note}
          />
          <button className="pa2 code f4">
            {this.state.id ? "Update note" : "Create note"}
          </button>
        </form>
        <div>
          {notes.map(item => (
            <div key={item.id} className="flex items-center">
              <li
                className="list pa1 f3"
                onClick={() => this.handleSetNote(item)}
              >
                {item.note}
                <button
                  className="bg-transparent bn f4"
                  onClick={() => this.handleNoteDelete(item.id)}
                >
                  <span>&times;</span>
                </button>
              </li>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default withAuthenticator(App, { includeGreetings: true });
